import paramiko
import sys

host = sys.argv[1]
port = 22
username = sys.argv[2]
password = sys.argv[3]

command = """ ip -o link | awk '$2 != "lo:" && $2 != "br0:" {print $2, $(NF-2)}'"""

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(host, port, username, password)


print("connecting............")
stdin, stdout, stderr = ssh.exec_command(command)
lines = stdout.readlines()
#print(lines)


macList=[]
intList = []
mgmt_macList=[]
mgmt_intList = []
bridge_macList=[]
bridge_intList = []

fw = open('macAdds.txt', 'w')
fpm = open('macAdds.properties', 'w')
fpi = open('int.properties', 'w')

for line in lines:
    fw.write(line)
    macFind = line.split(" ")
    macList.append(macFind[1].replace("\n", ""))
    intList.append(macFind[0].replace(":", ""))
    if "em" in line:
        mgmt_macFind = line.split(" ")
        mgmt_macList.append(mgmt_macFind[1].replace("\n", ""))
        mgmt_intList.append(mgmt_macFind[0].replace(":", ""))
    else:
        bridge_macFind = line.split(" ")
        bridge_macList.append(bridge_macFind[1].replace("\n", ""))
        bridge_intList.append(bridge_macFind[0].replace(":", ""))        


fpm.write(",".join(macList))
fpi.write(','.join(intList))
    

print(macList)
print(intList)
print(mgmt_macList)
print(mgmt_intList)
print(bridge_macList)
print(bridge_intList)

    
   
            
                